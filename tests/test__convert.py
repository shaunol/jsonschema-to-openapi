"""
This module provides tests for the `jsonschema_to_openapi.convert` module.
"""

import pytest

from jsonschema_to_openapi import convert
from jsonschema_to_openapi import exceptions


# pylint: disable=protected-access


@pytest.mark.parametrize(
    "schema,target",
    [
        (
            {"type": "object", "$schema": None, "$id": None, "id": None},
            {"type": "object"},
        )
    ],
)
def test__strip_illegal_keywords(schema, target):
    assert convert._strip_illegal_keywords(schema) == target


@pytest.mark.parametrize(
    "schema,target",
    [
        (
            {"type": "object", "dependencies": {"credit_card": ["billing_address"]}},
            {
                "type": "object",
                "allOf": [
                    {
                        "oneOf": [
                            {"not": {"required": ["credit_card"]}},
                            {"required": ["credit_card", "billing_address"]},
                        ]
                    }
                ],
            },
        )
    ],
)
def test__convert_dependencies(schema, target):
    assert convert._convert_dependencies(schema) == target


@pytest.mark.parametrize(
    "type_,valid",
    [
        ("hodger", False),
        ("null", True),
        ("boolean", True),
        ("object", True),
        ("array", True),
        ("number", True),
        ("string", True),
        ("integer", True),
        (["object", "array"], True),
        (["object", "hodger"], False),
    ],
)
def test__validate_type(type_, valid):
    if not valid:
        with pytest.raises(exceptions.InvalidTypeError):
            convert._validate_type(type_)
    else:
        convert._validate_type(type_)


@pytest.mark.parametrize(
    "schema,target,error",
    [
        ({"type": "object"}, {"type": "object", "nullable": False}, None),
        ({"type": ["object"]}, {"type": "object", "nullable": False}, None),
        ({"type": ["null", "object"]}, {"type": "object", "nullable": True}, None),
        ({"type": ["object", "string"]}, None, exceptions.TooManyTypesError),
        ({"type": ["object", "string", "null"]}, None, exceptions.TooManyTypesError),
    ],
)
def test__convert_types(schema, target, error):
    if error:
        with pytest.raises(error):
            convert._convert_types(schema)
    else:
        assert convert._convert_types(schema) == target


@pytest.mark.parametrize(
    "schema,target",
    [
        (
            {"type": "object", "patternProperties": "*"},
            {
                "type": "object",
                "x-patternProperties": "*",
                "additionalProperties": True,
            },
        ),
        (
            {"type": "object", "additionalProperties": False},
            {"type": "object", "additionalProperties": False},
        ),
    ],
)
def test__convert_properties(schema, target):
    assert convert._convert_properties(schema) == target


@pytest.mark.parametrize("schema,target", [({"const": "hodger"}, {"enum": ["hodger"]})])
def test__rewrite_constants(schema, target):
    assert convert._rewrite_constants(schema) == target


@pytest.mark.parametrize(
    "schema,target",
    [
        (
            {
                "type": "object",
                "properties": {
                    "street_address": {"type": "string"},
                    "country": {"enum": ["United States of America", "Canada"]},
                },
                "if": {
                    "properties": {"country": {"const": "United States of America"}}
                },
                "then": {
                    "properties": {"postal_code": {"pattern": "[0-9]{5}(-[0-9]{4})?"}}
                },
                "else": {
                    "properties": {
                        "postal_code": {"pattern": "[A-Z][0-9][A-Z] [0-9][A-Z][0-9]"}
                    }
                },
            },
            {
                "type": "object",
                "properties": {
                    "street_address": {"type": "string"},
                    "country": {"enum": ["United States of America", "Canada"]},
                },
                "oneOf": [
                    {
                        "allOf": [
                            {
                                "properties": {
                                    "country": {"const": "United States of America"}
                                }
                            },
                            {
                                "properties": {
                                    "postal_code": {"pattern": "[0-9]{5}(-[0-9]{4})?"}
                                }
                            },
                        ]
                    },
                    {
                        "allOf": [
                            {
                                "not": {
                                    "properties": {
                                        "country": {"const": "United States of America"}
                                    }
                                }
                            },
                            {
                                "properties": {
                                    "postal_code": {
                                        "pattern": "[A-Z][0-9][A-Z] [0-9][A-Z][0-9]"
                                    }
                                }
                            },
                        ]
                    },
                ],
            },
        )
    ],
)
def test__rewrite_if_then_else(schema, target):
    assert convert._rewrite_if_then_else(schema) == target


@pytest.mark.parametrize(
    "schema,target",
    [
        (
            {"type": "number", "minimum": 0, "exclusiveMaximum": 100},
            {"type": "number", "minimum": 0, "maximum": 100, "exclusiveMaximum": True},
        ),
        (
            {"type": "number", "exclusiveMinimum": 0, "maximum": 100},
            {"type": "number", "minimum": 0, "exclusiveMinimum": True, "maximum": 100,},
        ),
    ],
)
def test__rewrite_exclusive_min_max(schema, target):
    assert convert._rewrite_exclusive_min_max(schema) == target


@pytest.mark.parametrize(
    "schema,target",
    [
        (
            {
                "anyOf": [{"$ref": "#/my_object"}, {"type": "null"}],
                "my_object": {"type": "string"},
            },
            {
                "anyOf": [{"$ref": "#/my_object"}],
                "nullable": True,
                "my_object": {"type": "string"},
            },
        )
    ],
)
def test___convert_any_of_null(schema, target):
    assert convert._convert_any_of_null(schema) == target


@pytest.mark.parametrize(
    "schema,target",
    [
        (
            {"type": "object", "anyOf": [{"additionalProperties": False}]},
            {"type": "object", "additionalProperties": False},
        ),
        (
            {"type": "object", "oneOf": [{"additionalProperties": False}]},
            {"type": "object", "additionalProperties": False},
        ),
        (
            {"type": "object", "allOf": [{"additionalProperties": False}]},
            {"type": "object", "additionalProperties": False},
        ),
        (
            {"type": "number", "anyOf": [{"minimum": 0, "maximum": 100}]},
            {"type": "number", "minimum": 0, "maximum": 100},
        ),
        (
            {"type": "number", "anyOf": [{"minimum": 0}], "allOf": [{"maximum": 100}]},
            {"type": "number", "minimum": 0, "maximum": 100},
        ),
    ],
)
def test__avoid_x_ofs(schema, target):
    assert convert._avoid_x_ofs(schema) == target


@pytest.mark.parametrize(
    "schema,target",
    [
        (
            {
                "anyOf": [{"$ref": "#/definitions/my_object"}, {"type": "null"}],
                "definitions": {"my_object": {"type": "string", "const": "hodger",}},
            },
            {
                "nullable": True,
                "$ref": "#/definitions/my_object",
                "definitions": {
                    "my_object": {
                        "type": "string",
                        "enum": ["hodger"],
                        "nullable": False,
                    },
                },
            },
        ),
        (
            {
                "allOf": [
                    {"$ref": "#/definitions/nullable_schema"},
                    {"type": "object"},
                ],
                "definitions": {
                    "nullable_schema": {
                        "type": ["null", "object"],
                        "properties": {
                            "my_string": {"type": "string", "const": "hodger"}
                        },
                        "required": ["my_string"],
                        "additionalProperties": False,
                    }
                },
            },
            {
                "allOf": [
                    {"$ref": "#/definitions/nullable_schema"},
                    {"type": "object", "additionalProperties": True, "nullable": False},
                ],
                "definitions": {
                    "nullable_schema": {
                        "nullable": True,
                        "type": "object",
                        "properties": {
                            "my_string": {
                                "type": "string",
                                "enum": ["hodger"],
                                "nullable": False,
                            }
                        },
                        "required": ["my_string"],
                        "additionalProperties": False,
                    }
                },
            },
        ),
        (
            [{"type": "object"}],
            [{"type": "object", "nullable": False, "additionalProperties": True}],
        ),
        (
            {
                "type": "array",
                "items": {
                    "allOf": [
                        {
                            "type": "object",
                            "required": ["a", "b",],
                            "properties": {
                                "a": {
                                    "allOf": [{"type": "string"}, {"const": "hodger"}]
                                },
                                "b": {"type": ["null", "string"]},
                            },
                        },
                        {
                            "type": "object",
                            "required": ["c"],
                            "properties": {"c": {"type": ["null", "string"]}},
                        },
                    ]
                },
            },
            {
                "type": "array",
                "items": {
                    "allOf": [
                        {
                            "type": "object",
                            "required": ["a", "b"],
                            "properties": {
                                "a": {
                                    "allOf": [
                                        {"type": "string", "nullable": False},
                                        {"enum": ["hodger"]},
                                    ],
                                },
                                "b": {"nullable": True, "type": "string"},
                            },
                            "nullable": False,
                            "additionalProperties": True,
                        },
                        {
                            "type": "object",
                            "required": ["c"],
                            "properties": {"c": {"nullable": True, "type": "string"},},
                            "nullable": False,
                            "additionalProperties": True,
                        },
                    ]
                },
                "nullable": False,
            },
        ),
    ],
)
def test_convert(schema, target):
    assert convert.convert(schema) == target
